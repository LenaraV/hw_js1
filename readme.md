Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
Почему объявлять переменную через var считается плохим тоном?

1. если переменная не будет изменяться в процессе и имеет постоянное фиксированное значение - задаем ее через const, в противном случае переменная будет принимать разные значения - тогда используем let
var - я поняла, что проблема в этом способе объявления переменной в том, что она ограничена областью видимости - если ее задать внутри функции или объекта, другой кусок кода, написанный выше или ниже, ее не увидит эту переменную (var), и к ней нельзя обратиться/использовать еще где-то - а только в рамках той области, где ее и объявили